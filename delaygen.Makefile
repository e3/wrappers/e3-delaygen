where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile



## Exclude linux-ppc64e6500
##EXCLUDE_ARCHS = linux-ppc64e6500


APP:=delaygenApp
APPDB:=$(APP)/Db
APPSRC:=$(APP)/src


USR_INCLUDES += -I$(where_am_I)$(APPSRC)

SOURCES += $(APPSRC)/nigpibInterpose.c
SOURCES += $(APPSRC)/drvAsynDG645.cpp
SOURCES += $(APPSRC)/drvAsynColby.cpp
SOURCES += $(APPSRC)/devDG535.c
SOURCES += $(APPSRC)/drvAsynCoherentSDG.cpp

DBDS += $(APPSRC)/delaygenSupport.dbd


TEMPLATES += $(wildcard $(APPDB)/*.db)
TEMPLATES += $(wildcard $(APPDB)/*.proto*)


.PHONY: vlibs
vlibs:
